from matrix_bot_api.matrix_bot_api import MatrixBotAPI
from matrix_client.client import Room
from trello import TrelloClient
from datetime import datetime, timedelta
from config import (BOARD_ID, API_KEY, API_SECRET, TOKEN, TOKEN_SECRET,
    USERNAME, PASSWORD, SERVER, ROOM, ACTIONS_LIMIT, REFRESH_TIME)
import threading


def notify(oh_board, room, ACTIONS_LIMIT, last_activity_time):
    actions_list = []
    # quering trello api
    actions_list.append(oh_board.fetch_actions("addMemberToCard", action_limit=ACTIONS_LIMIT,  since=last_activity_time))
    actions_list.append(oh_board.fetch_actions("commentCard", action_limit=ACTIONS_LIMIT,  since=last_activity_time))
    actions_list.append(oh_board.fetch_actions("createCard", action_limit=ACTIONS_LIMIT,  since=last_activity_time))
    actions_list.append(oh_board.fetch_actions("updateCard", action_limit=ACTIONS_LIMIT,  since=last_activity_time))
    actions_list.append(oh_board.fetch_actions("addAttachmentToCard", action_limit=ACTIONS_LIMIT,  since=last_activity_time))

    for action in actions_list:
        for item in action:
            card_name = item['data']['card']['name']
            member_name = item['memberCreator']['fullName']
            if item['type'] == "addMemberToCard":
                new_member_name = item['member']['fullName']
                message = '{0} додає {1} до тікета "{2}"'.format(member_name, new_member_name, card_name)
                send(room, message)
            elif item['type'] == "commentCard":
                comment = item['data']['text']
                message = '{0} дописує коментар до тікета "{1}": {2}'.format(member_name, card_name, comment)
                send(room, message)
            elif item['type'] == "createCard":
                message = '{0} створює новий тікет "{1}"'.format(member_name, card_name)
                send(room, message)
            elif item['type'] == "updateCard":
                if 'pos' in item['data']['old']:
                    column_name = item['data']['list']['name']
                    message = '{0} переносить тікет "{1}" в колонку {2}'.format(member_name, card_name, column_name)
                    send(room, message)
                elif 'desc' in item['data']['old']:
                    desc = item['data']['card']['desc']
                    message = '{0} оновлює опис тікета "{1}": {2}'.format(member_name, card_name, desc)
                    send(room, message)
                # else:
                #     room.send_html('{0} оновлює тікет {1}'.format(member_name, card_name))
            elif item['type'] == "addAttachmentToCard":
                attachment_name = item['data']['attachment']['name']
                message = '{0} кріпить до тікета "{1}" файл "{2}"'.format(member_name, card_name, attachment_name)
                if 'url' in item['data']['attachment']:
                    attachment_link = item['data']['attachment']['url']
                    message +=', що доступний за посиланням: {0}'.format(attachment_link)
                send(room, message)
    
    last_activity_time = oh_board.get_last_activity() + timedelta(seconds=10)
    threading.Timer(REFRESH_TIME, notify, [oh_board, room, ACTIONS_LIMIT, last_activity_time]).start()


def send(room, message):
    room.send_notice(message)

def main():
    client = TrelloClient(
    api_key=API_KEY,
    api_secret=API_SECRET,
    token=TOKEN,
    token_secret=TOKEN_SECRET
    )

    # get OH.MY.GO!D board
    oh_board = client.get_board(BOARD_ID)

    last_activity_time = oh_board.get_last_activity() + timedelta(seconds=10)

    matrix_bot = MatrixBotAPI(USERNAME, PASSWORD, SERVER)
    room = Room(matrix_bot.client, ROOM)
    notify(oh_board, room, ACTIONS_LIMIT, last_activity_time)
